/*! \file eso.hpp
 * \brief a proprietary interface standard for differential-algebraic
 * equation systems
 *
 * ( C ) 2004 All rights reserved, Lars von Wedel, Georg Schopfer
 *  Lehrstuhl f�r Prozesstechnik, RWTH Aachen,
 */

#ifndef NUMR_ESO_HPP
#define NUMR_ESO_HPP

#pragma warning( disable : 4786)

//#ifdef WIN32
//#undef LINKDLL
//#ifdef MAKE_ESO_DLL
//#define LINKDLL __declspec(dllexport)
//#else
//#define LINKDLL __declspec(dllimport)
//#endif
//#else
//#define LINKDLL
//#endif


//#include <cstdlib>
#include <vector>
#include <iostream>
#include <string>
#include "DLLDefinesEsoCommon.hpp"

using namespace std;

namespace LptNumerics
{
    namespace Eso
    {
        /*! \brief an abstract interface for algebraic equation systems.
         *
         * The interface enables access to general differential-algebraic
         * systems f( x ). They might be square, but they don't have to. This
         * interface is reduced to the bare minimum( more or less ) required
         * to solve a differential-algebraic system. Extensions are expected
         * to be realized in appropriate subclasses.
         *
         * \author Jan Oldenburg, Martin Schlegel, Jutta Wyes, Lars von
         * Wedel, Georg Schopfer;
         */
        class ESOCOMMON_EXPORT Eso
        {
        public:

            Eso(){};
             virtual ~Eso(){};
            /*! \brief get variable names
             *
             * The method returns all variable names
             *
             * \param names - a field of names - normally the vector names is
             *empty, the variable names are appended at the end of names
             */
             virtual void get_variable_names( std::vector < std::string > &
                                             names ) = 0;


             virtual long get_num_diff_vars();

             virtual void get_diff_var_indices( long* ret_indices );

             virtual void get_indices_from_variable_names(
                const vector <string>& names, vector < long > & indices ){}
             virtual void get_variable_names_from_indices(
                const vector <long>& indices, vector < string > & names ){}

            /*! \brief set variable names
             *
             * The method sets all variable names
             *

             * \param indices - contains the indices of eso variables to
             * which the names are stored

             * \param names - a field of names corresponding to indices if
             * indices.size() != 0 name[indices[i]] := names[i]; // i=
             * 0..indices.size() else name[i] := names[i]; // i=
             * 0..names.size()
             */
             virtual void set_variable_names( const std::vector < long > &
                                             indices, const std::vector < std::string > & names ) = 0;

            /*! \name Equation set structure
             *
             * The equation set is defined as
             *
             *   f( x, x_dot, t ) = 0
             *
             * where the equations f constrain the variables x of the
             * system. Some or all variables can occur as dynamic states
             * with respect to time t.
             */
            //@{

            /*! \brief obtain the number of variables
             *
             * The method obtains the number of variables in the equation
             * set. The method returns 0( since no problem can occur ).
             * All variables are scalars. Vector variables are not considered
             * in this numerical formulation of the problem.
             *
             * \return the number of variables after the call
             */
             virtual long get_num_vars()const = 0;

            /*! \brief obtain the number of equations in the equation set
             *
             * The method obtains the number of equations in the equation
             * set.
             *
             * \return the number of equations
             */
             virtual long get_num_eqns()const = 0;

            /*! \brief check whether derivative information is provided
             *
             * The method checks, whether the equation system can supply
             * information about its derivatives, i.e. which derivatives
             * are structurally singular and which aren't. In case of an
             * 'elementary' equation system, it is likely that either all
             * or no information is known. However, if an equation system
             * is composed from different sources which supply different
             * information, it is possible that the structure is only
             * known partially. Further, in case of integrating function
             * calls that do not supply derivatives, it is possible to
             * know the full jacobian structure, but not all of its
             * values.
             *
             * \return a code for the information provided:
             *     0 - jacobian structure and values are unknown.
             *     1 - exact structure of jacobian but not all values
             *     2 - exact jacobian structure and all derivatives are provided
             */
             virtual short has_jacobian_info() = 0;

            /*! \brief check whether derivative information is provided
             *
             * The method checks, whether the equation system can supply
             * information about its derivatives w.r.t df/dt.
             * The interpretation of the info values is analogous to the
             * corresponding method of an algebraic system.
             *
             * \return info - contains a code for the information provided:
             *   0 - jacobian structure and values are unknown.
             *   1 - exact structure but not all values( or even none )
             *   2 - exact structure and all derivatives are provided
             */
             virtual short has_diff_jacobian_info() = 0;

            /*! \brief check number of entries in jacobian matrix
             *
             * The method determines the number of elements in the
             * jacobian matrix so that the caller can reserve
             * an appropriate chunk of memory. The method only runs
             * successfully, if a call to has_jacobian_info yields 0
             * or 1 as a result in the info parameter.
             *
             * \return the number of jacobian entries
             */
             virtual long get_num_jacobian_entries() = 0;

            /*! \brief check number of entries in differential jacobian matrix
             *
             * The method determines the number of elements in the
             * differential jacobian matrix with respect to the
             * derivatives, i.e. df/d dot( x ). where dot( x ) = dx/dt
             * and t is the independent variable. The caller can then
             * reserve an appropriate chunk of memory to hold the
             * structure or the values of the jacobian matrix. The method
             * only runs successfully, if a call to
             * has_diff_jacobian_info yields 0 or 1 as a result in the
             * info parameter.
             *
             * \return the number of jacobian entries
             */
             virtual long get_num_diff_jacobian_entries() = 0;

            /*! \brief obtain the structure of the jacobian matrix
             *
             * The method obtains information about the jacobian matrix (
             * the derivatives of f w.r.t x ). The information returned
             * comprises the location of entries in the jacobian matrix
             * and a flag indicating whether they can be computed. The
             * caller of the method has to allocate space in appropriate
             * dimensions in the rows, cols, and computed parameters. The
             * method only runs successfully, if a call to
             * has_jacobian_info yields 0 or 1 as a result in the info
             * parameter.
             *
             * \param len - the memory available in rows, cols, and computed;
             *     the method will write no more than len elements
             * \param rows - holds the rows of the elements after the call
             * \param cols - holds the columns of the elements after the call
             * \param computed - >0, when the corresponding element can be
             *     computed by the system, 0 otherwise
             */
             virtual void get_jacobian_struct( long len, long* rows, long*
                                              cols, short* computed ) = 0;

            /*! \brief obtain the structure of the differential jacobian matrix
             *
             * The method obtains information about the jacobian matrix (
             * the derivatives of f w.r.t dx/dt ). The information
             * returned comprises the location of entries in the jacobian
             * matrix and a flag indicating whether they can be computed.
             * The caller of the method has to allocate space in
             * appropriate dimensions in the rows, cols, and computed
             * parameters. The method only runs successfully, if a call
             * to has_diff_jacobian_info yields 0 or 1 as a result in the
             * info parameter.
             *

             * \param len - the memory available in rows, cols, and
             *     computed; the method will write no more than len
             *     elements
             * \param rows - holds the rows of the elements after the
             * call
             * \param cols - holds the columns of the elements after the
             * call
             * \param computed - indicates, whether the corresponding
             *     element can be computed( TRUE ), or not( FALSE )
             */
             virtual void get_diff_jacobian_struct( long len, long* rows,
                                                   long* cols, short* computed ) = 0;

            //@}

            /*! \name Equation system evaluation
             *
             * The following methods provide functionality to evaluate the
             * equation system. This includes setting or obtaining the values
             * of the variables or their derivatives and setting or obtaining
             * the corresponding derivatives.
             */
            //@{

            /*! \brief get variables values
             *
             * The method obtains the current values of the variables x of
             * the system f( x ). The method allows to retrieve all values at
             * once, or to select individual values of the overall set. This
             * might reduce data transfer if large parts of the system have
             * already been converged.
             *
             * \param len - the amount of storage available in the values
             *     parameter. The valid range is from 0 to get_num_vars-1

             * \param indices - the indices of the variable values to be
             *    retrieved ( an index ranges from 0 to get_num_vars - 1 ).
             *    If the parameter contains a NULL pointer, the first 'len'
             *    elements from the values vector are returned.

             * \param values - contains the requested values in the same
             *     order as referred to in 'indices'. The caller is
             *     responsible for the memory management of the array.
             */
             virtual void get_variables( long len, long* indices, double*
                                        values ) = 0;

            /*! \brief set variable values
             *
             * The method sets the new values for the variables x of the
             * system f( x ). The method allows to set all values at
             * once, or to select individual values of the overall system
             * to be set. This might reduce data transfer if large parts
             * of the\ system have already been converged.
             *
             * \param len - the count of values available in the values
             *     parameter. The valid range is from 0 to get_num_vars-1

             * \param indices - the indices of the variable values to be
             *    set ( an index ranges from 0 to get_num_vars - 1 ). If
             *    the parameter contains a NULL pointer, the first 'len'
             *    elements from the values vector are returned.

             * \param values - contains the values to be set in the same
             *     order as referred to in 'indices'. The caller is
             *     responsible for the memory management of the array.
             */
             virtual void set_variables( long len, long* indices, const 
										double* values ) = 0;

            /*! \brief get differential variables values
             *
             * The method obtains the current values of the terms dx/dt
             * of the system f( x, dx/dt, t ). Though, these terms do not
             * occur as variables per se in the system, they can be
             * treated as such because the solver will establish a
             * correspondence between x and dx/dt, e.g. via BDF methods.
             *
             * The method allows to retrieve all values at once, or to
             * select individual values of the overall set. This might
             * reduce data transfer if large parts of the system have
             * already been converged.
             *

             * \param len - the amount of storage available in the values
             *     parameter. The valid range is from 0 to get_num_vars-1
             *     \param indices - the indices of the diff. variable
             *     values to be retrieved ( an index ranges from 0 to
             *     get_num_vars - 1 ). If the parameter contains a NULL
             *     pointer, the first 'len' elements from the values
             *     vector are returned.

             * \param values - contains the requested values in the same
             *     order as referred to in 'indices'. The caller is
             *     responsible for the memory management of the array.
             */
             virtual void get_derivatives( long len, long* indices, double*
                                          values ) = 0;

            /*! \brief set differential variables values
             *
             * The method sets the new values for the variables dx/dt of
             * the system f( x, dx/dt, t ). The method allows to set all
             * values at once, or to select individual values of the
             * overall system to be set. This might reduce data transfer
             * if large parts of the\ system have already been converged.
             *

             * \param len - the count of values available in the values
             *     parameter. The valid range is from 0 to get_num_vars-1

             * \param indices - the indices of the variable values to be
             *    set ( an index ranges from 0 to get_num_vars - 1 ). If
             *    the parameter contains a NULL pointer, the first 'len'
             *    elements from the values vector are returned.

             * \param values - contains the values to be set in the same
             *     order as referred to in 'indices'. The caller is
             *     responsible for the memory management of the array.
             */
             virtual void set_derivatives( long len, long* indices, double*
                                          values ) = 0;

            /*! \brief set value of the independent variable
             *
             * The method sets the value of the independent variable of
             * the differential-algebraic equation system t.Though this
             * is written as t here, the formulation is not limited to
             * time as an independent variable.
             *
             * \return the new value of the independent variable
             */
             virtual double get_independent_var() = 0;

            /*! \brief get value of the independent variable
             *
             * The method obtains the value of the independent variable
             * of the differential-algebraic equation system t. Though
             * this is written as t here, the formulation is not limited
             * to time as an independent variable.
             *

             * \param val - contains the value of the independent
             *     variable after termination of the call
             */
             virtual void set_independent_var( double val ) = 0;

            /*! \brief get variable bounds
             *
             * The method obtains the bounds of the variables x.
             *

             * \param len - the count of values available in the lower
             *     and upper parameter. The valid range is from 0 to
             *     get_num_vars-1

             * \param indices - the indices of the variable bounds to be
             *    obtained ( an index ranges from 0 to get_num_vars - 1
             *    ). If the parameter contains a NULL pointer, the first
             *    'len' elements from the upper and lower vectors are
             *    returned.

             * \param upper - contains the upper bounds values to be
             *     obtained in the same order as referred to in
             *     'indices'. The caller is responsible for the memory
             *     management of the array.

             * \param lower - contains the lower bounds values to be
             *     obtained in the same order as referred to in
             *     'indices'. The caller is responsible for the memory
             *     management of the array.
             */
             virtual void get_bounds( long len, long* indices, double* lower,
                                     double* upper ) = 0;

            /*! \brief set variable bounds
             *
             * The method sets the bounds of the variables x.
             *
             * \param len - the count of values available in the lower
             *     and upper parameter. The valid range is from 0 to
             *     get_num_vars-1
             * \param indices - the indices of the variable bounds to be
             *    set ( an index ranges from 0 to get_num_vars - 1 ). If
             *    the parameter contains a NULL pointer, the first 'len'
             *    elements from the upper and lower vectors are returned.
             * \param upper - contains the upper bounds values to be set
             *     in the same order as referred to in 'indices'. The
             *     caller is responsible for the memory management of the
             *     array.
             * \param lower - contains the lower bounds values to be set
             *     in the same order as referred to in 'indices'. The
             *     caller is responsible for the memory management of the
             *     array.
             */
             virtual void set_bounds( long len, long* indices, double* lower,
                                     double* upper ) = 0;

            /*! \brief get residuals
             *
             * The method determines residuals for the current state of the
             * variables x. Either all or some specific elements can be
             * obtained. The caller is responsible for the parameter memory
             * management.
             *
             * \param len - the memory available in 'residuals'
             * \param indices - if the parameter is a NULL pointer,
             *     the method will return the first 'len' residuals
             *     in 'residuals'. Otherwise, the 'len' elements referred
             *     to will be provided.
             * \param residuals - holds the residual values requested on
             *     successful termination.
             */
             virtual void get_residuals( long len, long* indices, double*
                                        residuals ) = 0;

            /*! \brief obtain the values of derivatives
             *
             * The method obtains derivative values of f w.r.t. x
             * of the elements requested. Either all or some specific
             * elements can be queried. The caller is responsible to
             * manage memory for the parameters.
             *
             * \param len - the memory available in jacobian
             * \param indices - if the parameter is a NULL pointer,
             *     the method will return the first 'len' derivatives
             *     in 'jacobian'. Otherwise, the len elements referred
             *     to will be provided.
             * \param jacobian - holds the jacobian values requested on
             *     successful termination.
             */
             virtual void get_jacobian_values( long len, long* indices,
                                              double* jacobian ) = 0;

            /*! \brief obtain the values of differential jacobian
             *
             * The method obtains derivative values of f w.r.t. dx/dt
             * of the elements requested. Either all or some specific
             * elements can be queried. The caller is responsible to
             * manage memory for the parameters.
             *
             * \param len - the memory available in jacobian
             * \param indices - if the parameter is a NULL pointer,
             *     the method will return the first 'len' derivatives
             *     in 'jacobian'. Otherwise, the len elements referred
             *     to will be provided.
             * \param diff_jacobian - holds the jacobian values requested on
             *     successful termination.
             */
             virtual void get_diff_jacobian_values( long len, long* indices,
                                                   double* diff_jacobian ) = 0;
							   // input: names
	   // ouptut: indices of names in the eso, -1, if the name does not exist
	    virtual void get_var_nums(vector<string> names, long * indices);

	   // input: len of indices
	   // input: indices of equations
	   // output: names of equations
	    virtual void get_eqn_names(long len, long* indices, vector<string>& names);


	    virtual long get_num_params() const;
	    virtual void get_param_indices(long * indices) const;

	   // output: sens array of dx/dp
	   // undefined, if no set_var_sensitivities is used before
	   virtual void get_var_sensitivities(double* sens);
	   // input: sens array of dx/dp
	    virtual void set_var_sensitivities(double* sens);

        virtual long get_num_assigned_vars() const;
	    virtual void get_assigned_variables(long * indices);

	   // dummy: return 0
	    virtual long get_num_stns();

		 // input: multivector_gradient is given from integrator
		 //				 with size = nv x np, nv is number of variables
		 //				 np: number of parameters
		 //				 the storage order of the input is:   
		 //					[dv1/dp1, dv1/dp2, ... dv1/dpnp, dv2/dp1, ..., dvnv/dpnp]
		 // output: directional_derivs directional derivative with size = ne x np,
		 //        ne is number of equations
		 //				 the storage order of the output is:   
		 //					[de1/dp1, de1/dp2, ... de1/dpnp, de2/dp1, ..., dene/dpnp]
		 // directional_derivs := jacobian values x multivector
		  virtual void get_jac_mult_multivector(
			 double* multivector_gradient,
			 long np,
			 double* directional_derivs);

		 // multivector * Hesse * multivector + Jac * multimatrix
		 virtual void get_hesse_mult_multivector(
			 double* first_order_sensitivities, // input dx/dp
			 long np, // input number of parameters
			 double* second_order_sensitivities, // input d2x/dpdq
			 double* two_directional_derivs //output d2F/dpdq
			 );


            /*! \brief writes eso to stream
             *
             *  The method writes this eso with name "name" in format
             *  "text" or "xml" to the stream s. The overloaded operator
             *  << works with output_to_stream from the current class
             *  object. This function is stored in lptNumerics and is
             *  overloadable from subclasses from Eso
             *
             * \param s - the output is written to s
             * \param name - titel of the eso or NULL
             * \param format - is 1 for output in text format is 2 for
             *               output in xml format, (2 is not
             *               implemented) \return the changed output
             *               stream s
             */
         virtual ostream& output_to_stream( ostream& s, const char* name, short format );


            //@}

        };
        ESOCOMMON_EXPORT void get_indices_of_names_in_eso( const vector < string > *
                                                  names, Eso* eso, long* indices, long& len );
    }
};

/*! \brief output of an eso: variable values, residuals and jacobian values
 *
 *  usage:  cout << my_eso;
 *
 *  \param out - goal of output
 *  \param eso - pointer to Eso
 *  \return the changed stream parameter out
 */
ESOCOMMON_EXPORT ostream& operator << ( ostream& out, LptNumerics::Eso::Eso* eso );


#endif
