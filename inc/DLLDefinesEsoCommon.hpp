#ifndef _DLLDEFINES_ESOCOMMON_H_
#define _DLLDEFINES_ESOCOMMON_H_

// We are using the Visual Studio Compiler and building Shared libraries

// Determine whether system is Windows
#ifdef MAKE_ESO_COMMON_DLL
#define ESOCOMMON_EXPORT __declspec(dllexport)
#else
#ifdef WIN32
#define ESOCOMMON_EXPORT __declspec(dllimport)
#else 
#define ESOCOMMON_EXPORT
#endif
#endif

#endif /* _DLLDEFINES_ESOCOMMON_H_ */