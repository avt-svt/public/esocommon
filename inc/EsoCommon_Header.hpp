#ifndef __ESO_COMMON_HEADER_HPP__
#define __ESO_COMMON_HEADER_HPP__

#include <math.h>
#include <vector>
#include <float.h>
#include <iostream>
#include "DLLDefinesEsoCommon.hpp"
#include "jsp.hpp"

//#include "Array.hpp" // for counting the maximal accessed index of cs, fds and ids and then used no more time
using namespace std;

static int cs[1000];
static int cs_c=0;
static double fds[1000];
static double t2_fds[1000];
static int fds_c=0;
static int ids[1000];
static int ids_c=0;


// here are utility functions for acsammmeso, not generated
ESOCOMMON_EXPORT void jsp_acs_sum(jsp* x, int & len, jsp& sum_x);
ESOCOMMON_EXPORT void jsp_acs_pow(jsp & a, jsp & x, jsp & v_pow);
ESOCOMMON_EXPORT void jsp_acs_div(jsp & top_arg, jsp & bottom_arg, jsp & v_div);
ESOCOMMON_EXPORT void jsp_acs_sqrt(jsp & arg, jsp & v_sqrt);
ESOCOMMON_EXPORT void jsp_acs_ln(jsp & arg, jsp & v_ln);
ESOCOMMON_EXPORT void jsp_acs_exp(jsp & arg, jsp & v_exp);
ESOCOMMON_EXPORT void jsp_acs_tanh(jsp& x, jsp& v_tanh);

ESOCOMMON_EXPORT void jsp_t1_acs_sum(jsp* x, jsp* t1_x, int& len, jsp& sum_x, jsp& t1_sum_x);
ESOCOMMON_EXPORT void jsp_t1_acs_pow(jsp& a, jsp& t1_a, jsp& x, jsp& t1_x, jsp& v_pow, jsp& t1_v_pow); // for sparsity pattern for Hessian
ESOCOMMON_EXPORT void jsp_t1_acs_div(jsp& top_arg, jsp& t1_top_arg, jsp& bottom_arg, jsp& t1_bottom_arg, jsp& v_div, jsp& t1_v_div);
ESOCOMMON_EXPORT void jsp_t1_acs_sqrt(jsp& arg, jsp& t1_arg, jsp& v_sqrt, jsp& t1_v_sqrt);
ESOCOMMON_EXPORT void jsp_t1_acs_ln(jsp& arg, jsp& t1_arg, jsp& v_ln, jsp& t1_v_ln);
ESOCOMMON_EXPORT void jsp_t1_acs_exp(jsp& arg, jsp& t1_arg, jsp& v_exp, jsp& t1_v_exp);
ESOCOMMON_EXPORT void jsp_t1_acs_tanh(jsp& x, jsp& t1_x, jsp& v_tanh, jsp& t1_v_tanh);

ESOCOMMON_EXPORT double dcc_log(double& x);
ESOCOMMON_EXPORT void acs_sum(double* x, int & len, double & sum_x);
ESOCOMMON_EXPORT void acs_pow(double & a, double & x, double & v_pow);
ESOCOMMON_EXPORT void acs_div(double & top_arg, double & bottom_arg, double & v_div);
ESOCOMMON_EXPORT void acs_sqrt(double & arg, double & v_sqrt);
ESOCOMMON_EXPORT void acs_ln(double & arg, double & v_ln);
ESOCOMMON_EXPORT void acs_exp(double & arg, double & v_exp);
ESOCOMMON_EXPORT void acs_tanh(double & x, double & v_tanh);
ESOCOMMON_EXPORT void t1_acs_sum(double* x, double* t1_x, int& len, double& sum_x, double& t1_sum_x);
ESOCOMMON_EXPORT void t1_acs_pow(double& a, double& t1_a, double& x, double& t1_x, double& v_pow, double& t1_v_pow);
ESOCOMMON_EXPORT void t1_acs_div(double& top_arg, double& t1_top_arg, double& bottom_arg, double& t1_bottom_arg, double& v_div, double& t1_v_div);
ESOCOMMON_EXPORT void t1_acs_sqrt(double& arg, double& t1_arg, double& v_sqrt, double& t1_v_sqrt);
ESOCOMMON_EXPORT void t1_acs_ln(double& arg, double& t1_arg, double& v_ln, double& t1_v_ln);
ESOCOMMON_EXPORT void t1_acs_exp(double& arg, double& t1_arg, double& v_exp, double& t1_v_exp);
ESOCOMMON_EXPORT void t1_acs_tanh(double& x, double& t1_x, double& v_tanh, double& t1_v_tanh);
ESOCOMMON_EXPORT void a1_acs_sum(int bmode_1, double* x, double* a1_x, int& len, double& sum_x, double& a1_sum_x);
ESOCOMMON_EXPORT void a1_acs_pow(int bmode_1, double& a, double& a1_a, double& x, double& a1_x, double& v_pow, double& a1_v_pow);
ESOCOMMON_EXPORT void a1_acs_div(int bmode_1, double& top_arg, double& a1_top_arg, double& bottom_arg, double& a1_bottom_arg, double& v_div, double& a1_v_div);
ESOCOMMON_EXPORT void a1_acs_sqrt(int bmode_1, double& arg, double& a1_arg, double& v_sqrt, double& a1_v_sqrt);
ESOCOMMON_EXPORT void a1_acs_ln(int bmode_1, double& arg, double& a1_arg, double& v_ln, double& a1_v_ln);
ESOCOMMON_EXPORT void a1_acs_exp(int bmode_1, double& arg, double& a1_arg, double& v_exp, double& a1_v_exp);
ESOCOMMON_EXPORT void a1_acs_tanh(int bmode_1, double& arg, double& a1_arg, double& v_tanh, double& a1_v_tanh);
//ESOCOMMON_EXPORT void t2_t1_acs_sum(double* x, double* t2_x, double* t1_x, double* t2_t1_x, int& len, double& sum_x, double& t2_sum_x, double& t1_sum_x, double& t2_t1_sum_x);
//ESOCOMMON_EXPORT void t2_t1_acs_pow(double& a, double& t2_a, double& t1_a, double& t2_t1_a, double& x, double& t2_x, double& t1_x, double& t2_t1_x, double& v_pow, double& t2_v_pow, double& t1_v_pow, double& t2_t1_v_pow);
//ESOCOMMON_EXPORT void t2_t1_acs_div(double& top_arg, double& t2_top_arg, double& t1_top_arg, double& t2_t1_top_arg, double& bottom_arg, double& t2_bottom_arg, double& t1_bottom_arg, double& t2_t1_bottom_arg, double& v_div, double& t2_v_div, double& t1_v_div, double& t2_t1_v_div);
//ESOCOMMON_EXPORT void t2_t1_acs_sqrt(double& arg, double& t2_arg, double& t1_arg, double& t2_t1_arg, double& v_sqrt, double& t2_v_sqrt, double& t1_v_sqrt, double& t2_t1_v_sqrt);
//ESOCOMMON_EXPORT void t2_t1_acs_ln(double& arg, double& t2_arg, double& t1_arg, double& t2_t1_arg, double& v_ln, double& t2_v_ln, double& t1_v_ln, double& t2_t1_v_ln);
//ESOCOMMON_EXPORT void t2_t1_acs_exp(double& arg, double& t2_arg, double& t1_arg, double& t2_t1_arg, double& v_exp, double& t2_v_exp, double& t1_v_exp, double& t2_t1_v_exp);
//ESOCOMMON_EXPORT void t2_t1_acs_tanh(double& x, double& t2_x, double& t1_x, double& t2_t1_x, double& v_tanh, double& t2_v_tanh, double& t1_v_tanh, double& t2_t1_v_tanh);
ESOCOMMON_EXPORT void t2_a1_acs_sum(int bmode_1, double* x, double* t2_x, double* a1_x, double* t2_a1_x, int& len, double& sum_x, double& t2_sum_x, double& a1_sum_x, double& t2_a1_sum_x);
ESOCOMMON_EXPORT void t2_a1_acs_pow(int bmode_1, double& a, double& t2_a, double& a1_a, double& t2_a1_a, double& x, double& t2_x, double& a1_x, double& t2_a1_x, double& v_pow, double& t2_v_pow, double& a1_v_pow, double& t2_a1_v_pow);
ESOCOMMON_EXPORT void t2_a1_acs_div(int bmode_1, double& top_arg, double& t2_top_arg, double& a1_top_arg, double& t2_a1_top_arg, double& bottom_arg, double& t2_bottom_arg, double& a1_bottom_arg, double& t2_a1_bottom_arg, double& v_div, double& t2_v_div, double& a1_v_div, double& t2_a1_v_div);
ESOCOMMON_EXPORT void t2_a1_acs_sqrt(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_sqrt, double& t2_v_sqrt, double& a1_v_sqrt, double& t2_a1_v_sqrt);
ESOCOMMON_EXPORT void t2_a1_acs_ln(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_ln, double& t2_v_ln, double& a1_v_ln, double& t2_a1_v_ln);
ESOCOMMON_EXPORT void t2_a1_acs_exp(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_exp, double& t2_v_exp, double& a1_v_exp, double& t2_a1_v_exp);
ESOCOMMON_EXPORT void t2_a1_acs_tanh(int bmode_1, double& x, double& t2_x, double& a1_x, double& t2_a1_x, double& v_tanh, double& t2_v_tanh, double& a1_v_tanh, double& t2_a1_v_tanh);

ESOCOMMON_EXPORT bool cond_lock(int index, int* condit, int* lock, int* prev);

#endif // __ESO_COMMON_HEADER_HPP__
