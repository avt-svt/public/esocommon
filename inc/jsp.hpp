#ifndef JSP_INCLUDED_
#define JSP_INCLUDED_

#include<set>
#include<iostream>
#include<fstream>
#include <float.h>
#include "DLLDefinesEsoCommon.hpp"
using namespace std;

  class  jsp {
    public :
      double v; 
	  set<int> nz;
      ESOCOMMON_EXPORT jsp(const double& x);
      ESOCOMMON_EXPORT jsp();
      ESOCOMMON_EXPORT jsp& operator=(const jsp& x);
      ESOCOMMON_EXPORT jsp& operator=(const double& a);
  };

  // fixes the member nz of caller to x1.nz but the member v to a
  // This is needed for special cases in jsp_acs_functions
  jsp ESOCOMMON_EXPORT assign(const jsp& x1, const double& a);
  // fixes the member nz of caller to the union (x1.nz, x2.nz) but the member v to a
  // This is needed for special cases in jsp_acs_functions
  jsp ESOCOMMON_EXPORT assign(const jsp& x1, const jsp& x2, const double& a);

  jsp ESOCOMMON_EXPORT operator*(const jsp& x1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator*(const double& a1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator*(const jsp& x1, const double& a2);

  jsp ESOCOMMON_EXPORT operator+(const jsp& x1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator+(const double& a1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator+(const jsp& x1, const double& a2);

  jsp ESOCOMMON_EXPORT operator-(const jsp& x1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator-(const double& a1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator-(const jsp& x1, const double& a2);

  jsp ESOCOMMON_EXPORT operator/(const jsp& x1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator/(const double& a1, const jsp& x2);
  jsp ESOCOMMON_EXPORT operator/(const jsp& x1, const double& a2);

  bool ESOCOMMON_EXPORT operator<(const jsp& x1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator<(const double& a1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator<(const jsp& x1, const double& a2);

  bool ESOCOMMON_EXPORT operator<=(const jsp& x1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator<=(const double& a1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator<=(const jsp& x1, const double& a2);

  bool ESOCOMMON_EXPORT operator>(const jsp& x1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator>(const double& a1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator>(const jsp& x1, const double& a2);

  bool ESOCOMMON_EXPORT operator>=(const jsp& x1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator>=(const double& a1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator>=(const jsp& x1, const double& a2);

  bool ESOCOMMON_EXPORT operator==(const jsp& x1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator==(const double& a1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator==(const jsp& x1, const double& a2);

  bool ESOCOMMON_EXPORT operator!=(const jsp& x1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator!=(const double& a1, const jsp& x2);
  bool ESOCOMMON_EXPORT operator!=(const jsp& x1, const double& a2);


// prints jsp to a given file in exatly the same format as ADOL-C's methode jac_pat does. The diff operator can be used on both files.
  void ESOCOMMON_EXPORT print_jsp(ostream &tgtstream, jsp *spf, int n);

  jsp ESOCOMMON_EXPORT sin(const jsp& x);
  jsp ESOCOMMON_EXPORT cos(const jsp& x);
  jsp ESOCOMMON_EXPORT exp(const jsp& x);
  jsp ESOCOMMON_EXPORT atan(const jsp& x);

  jsp ESOCOMMON_EXPORT pow(const jsp& x, const double& c);
  jsp ESOCOMMON_EXPORT pow(const jsp& x1, const jsp& x2);
  jsp ESOCOMMON_EXPORT pow(const double& a1, const jsp& x2);

  jsp ESOCOMMON_EXPORT sqrt(const jsp& x);
  jsp ESOCOMMON_EXPORT log(const jsp& x);
  jsp ESOCOMMON_EXPORT tanh(const jsp& x);
  jsp ESOCOMMON_EXPORT jsp_dcc_log(const jsp& x);

  

#endif

