#include "EsoCommon_Header.hpp"
// ++++++++++++++++++++++++++++++++++
// + Code generated by dcc 1.0      +
// ++++++++++++++++++++++++++++++++++

void t2_a1_acs_sum(int bmode_1, double* x, double* t2_x, double* a1_x, double* t2_a1_x, int& len, double& sum_x, double& t2_sum_x, double& a1_sum_x, double& t2_a1_sum_x)
#pragma ad indep x t2_x a1_x t2_a1_x a1_sum_x t2_a1_sum_x
#pragma ad dep sum_x t2_sum_x a1_x t2_a1_x
{
   int i=0; 
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   double v1_2=0; 
   double t2_v1_2=0; 
   double a1_v1_2=0; 
   double t2_a1_v1_2=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      for (    i=0; i<len;    i=i+1) {
         // nonfloat assignment
         cs[cs_c]=0;
         // nonfloat assignment
         cs_c=cs_c+1;
         t2_v2_0=t2_sum_x; // TLM scalar memory reference
         v2_0=sum_x; // SAC scalar memory reference
         t2_fds[fds_c]=t2_v2_0; // TLM =
         fds[fds_c]=v2_0; // SAC =
         // nonfloat assignment
         fds_c=fds_c+1;
         t2_v2_0=t2_sum_x; // TLM scalar memory reference
         v2_0=sum_x; // SAC scalar memory reference
         t2_v2_1=t2_x[i]; // TLM array memory reference
         v2_1=x[i]; // SAC array memory reference
         t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
         v2_2=v2_0+v2_1; // SAC +
         t2_sum_x=t2_v2_2; // TLM =
         sum_x=v2_2; // SAC =
         // nonfloat assignment
         ids[ids_c]=i;
         // nonfloat assignment
         ids_c=ids_c+1;
      }
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            ids_c=ids_c-1;
            // nonfloat assignment
            i=ids[ids_c];
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_sum_x=t2_v2_0; // TLM =
            sum_x=v2_0; // SAC =
            t2_v2_0=t2_sum_x; // TLM scalar memory reference
            v2_0=sum_x; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t2_v2_0=t2_x[i]; // TLM array memory reference
            v2_0=x[i]; // SAC array memory reference
            t2_v1_1=t2_v2_0; // TLM =
            v1_1=v2_0; // SAC =
            t2_v2_0=t2_v1_0; // TLM scalar memory reference
            v2_0=v1_0; // SAC scalar memory reference
            t2_v2_1=t2_v1_1; // TLM scalar memory reference
            v2_1=v1_1; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_v1_2=t2_v2_2; // TLM =
            v1_2=v2_2; // SAC =
            t2_v2_0=t2_a1_sum_x; // TLM scalar memory reference
            v2_0=a1_sum_x; // SAC scalar memory reference
            t2_a1_v1_2=t2_v2_0; // TLM =
            a1_v1_2=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_sum_x=t2_v2_0; // TLM =
            a1_sum_x=v2_0; // SAC =
            t2_v2_0=t2_a1_v1_2; // TLM scalar memory reference
            v2_0=a1_v1_2; // SAC scalar memory reference
            t2_a1_v1_0=t2_v2_0; // TLM =
            a1_v1_0=v2_0; // SAC =
            t2_v2_0=t2_a1_v1_2; // TLM scalar memory reference
            v2_0=a1_v1_2; // SAC scalar memory reference
            t2_a1_v1_1=t2_v2_0; // TLM =
            a1_v1_1=v2_0; // SAC =
            t2_v2_0=t2_a1_x[i]; // TLM array memory reference
            v2_0=a1_x[i]; // SAC array memory reference
            t2_v2_1=t2_a1_v1_1; // TLM scalar memory reference
            v2_1=a1_v1_1; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_x[i]=t2_v2_2; // TLM =
            a1_x[i]=v2_2; // SAC =
            t2_v2_0=t2_a1_sum_x; // TLM scalar memory reference
            v2_0=a1_sum_x; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_sum_x=t2_v2_2; // TLM =
            a1_sum_x=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_sum(x,t2_x,len,sum_x,t2_sum_x);
   }
}
void t2_a1_acs_pow(int bmode_1, double& a, double& t2_a, double& a1_a, double& t2_a1_a, double& x, double& t2_x, double& a1_x, double& t2_a1_x, double& v_pow, double& t2_v_pow, double& a1_v_pow, double& t2_a1_v_pow)
#pragma ad indep a t2_a a1_a t2_a1_a x t2_x a1_x t2_a1_x a1_v_pow t2_a1_v_pow
#pragma ad dep v_pow t2_v_pow a1_a t2_a1_a a1_x t2_a1_x
{
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   double res_pow=0; 
   double t2_res_pow=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   double v2_3=0; 
   double t2_v2_3=0; 
   double v2_4=0; 
   double t2_v2_4=0; 
   double v2_5=0; 
   double t2_v2_5=0; 
   double v2_6=0; 
   double t2_v2_6=0; 
   double v2_7=0; 
   double t2_v2_7=0; 
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      // nonfloat assignment
      cs[cs_c]=0;
      // nonfloat assignment
      cs_c=cs_c+1;
      t2_v2_0=t2_v_pow; // TLM scalar memory reference
      v2_0=v_pow; // SAC scalar memory reference
      t2_fds[fds_c]=t2_v2_0; // TLM =
      fds[fds_c]=v2_0; // SAC =
      // nonfloat assignment
      fds_c=fds_c+1;
      t1_acs_pow(a,t2_a,x,t2_x,v_pow,t2_v_pow);
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_v_pow=t2_v2_0; // TLM =
            v_pow=v2_0; // SAC =
            t2_v2_0=t2_a; // TLM scalar memory reference
            v2_0=a; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t1_acs_pow(v1_0,t2_v1_0,x,t2_x,v1_1,t2_v1_1);
            t2_v2_0=t2_a1_v_pow; // TLM scalar memory reference
            v2_0=a1_v_pow; // SAC scalar memory reference
            t2_a1_v1_1=t2_v2_0; // TLM =
            a1_v1_1=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_v_pow=t2_v2_0; // TLM =
            a1_v_pow=v2_0; // SAC =
            t2_v2_0=t2_x; // TLM scalar memory reference
            v2_0=x; // SAC scalar memory reference
            t2_v2_1=0;  // TLM constant reference
            v2_1=1;  // SAC constant reference
            t2_v2_2=t2_v2_0-t2_v2_1; // TLM -
            v2_2=v2_0-v2_1; // SAC -
            t2_a1_v1_0=t2_v2_2; // TLM =
            a1_v1_0=v2_2; // SAC =
            t1_acs_pow(v1_0,t2_v1_0,a1_v1_0,t2_a1_v1_0,res_pow,t2_res_pow);
            t2_v2_0=t2_x; // TLM scalar memory reference
            v2_0=x; // SAC scalar memory reference
            t2_v2_1=t2_res_pow; // TLM scalar memory reference
            v2_1=res_pow; // SAC scalar memory reference
            t2_v2_2=v2_1*t2_v2_0+v2_0*t2_v2_1; // TLM *
            v2_2=v2_0*v2_1; // SAC *
            t2_v2_3=t2_a1_v1_1; // TLM scalar memory reference
            v2_3=a1_v1_1; // SAC scalar memory reference
            t2_v2_4=v2_3*t2_v2_2+v2_2*t2_v2_3; // TLM *
            v2_4=v2_2*v2_3; // SAC *
            t2_a1_v1_0=t2_v2_4; // TLM =
            a1_v1_0=v2_4; // SAC =
            t2_v2_0=t2_a1_x; // TLM scalar memory reference
            v2_0=a1_x; // SAC scalar memory reference
            t2_v2_1=t2_v1_1; // TLM scalar memory reference
            v2_1=v1_1; // SAC scalar memory reference
            t2_v2_2=t2_v1_0; // TLM scalar memory reference
            v2_2=v1_0; // SAC scalar memory reference
            t2_v2_3=t2_v2_2/v2_2; // TLM log
            v2_3=dcc_log(v2_2);  // SAC log
            t2_v2_4=v2_3*t2_v2_1+v2_1*t2_v2_3; // TLM *
            v2_4=v2_1*v2_3; // SAC *
            t2_v2_5=t2_a1_v1_1; // TLM scalar memory reference
            v2_5=a1_v1_1; // SAC scalar memory reference
            t2_v2_6=v2_5*t2_v2_4+v2_4*t2_v2_5; // TLM *
            v2_6=v2_4*v2_5; // SAC *
            t2_v2_7=t2_v2_0+t2_v2_6; // TLM +
            v2_7=v2_0+v2_6; // SAC +
            t2_a1_x=t2_v2_7; // TLM =
            a1_x=v2_7; // SAC =
            t2_v2_0=t2_a1_a; // TLM scalar memory reference
            v2_0=a1_a; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_a=t2_v2_2; // TLM =
            a1_a=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_pow(a,t2_a,x,t2_x,v_pow,t2_v_pow);
   }
}
void t2_a1_acs_div(int bmode_1, double& top_arg, double& t2_top_arg, double& a1_top_arg, double& t2_a1_top_arg, double& bottom_arg, double& t2_bottom_arg, double& a1_bottom_arg, double& t2_a1_bottom_arg, double& v_div, double& t2_v_div, double& a1_v_div, double& t2_a1_v_div)
#pragma ad indep top_arg t2_top_arg a1_top_arg t2_a1_top_arg bottom_arg t2_bottom_arg a1_bottom_arg t2_a1_bottom_arg a1_v_div t2_a1_v_div
#pragma ad dep v_div t2_v_div a1_top_arg t2_a1_top_arg a1_bottom_arg t2_a1_bottom_arg
{
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   double v1_2=0; 
   double t2_v1_2=0; 
   double a1_v1_2=0; 
   double t2_a1_v1_2=0; 
   double div_ret1=0; 
   double t2_div_ret1=0; 
   double div_ret2=0; 
   double t2_div_ret2=0; 
   double int_ret1=0; 
   double t2_int_ret1=0; 
   double one=0; 
   double t2_one=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   double v2_3=0; 
   double t2_v2_3=0; 
   double v2_4=0; 
   double t2_v2_4=0; 
   t2_v2_0=0;  // TLM constant reference
   v2_0=1.0;  // SAC constant reference
   t2_one=t2_v2_0; // TLM =
   one=v2_0; // SAC =
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      // nonfloat assignment
      cs[cs_c]=0;
      // nonfloat assignment
      cs_c=cs_c+1;
      t2_v2_0=t2_v_div; // TLM scalar memory reference
      v2_0=v_div; // SAC scalar memory reference
      t2_fds[fds_c]=t2_v2_0; // TLM =
      fds[fds_c]=v2_0; // SAC =
      // nonfloat assignment
      fds_c=fds_c+1;
      t1_acs_div(top_arg,t2_top_arg,bottom_arg,t2_bottom_arg,v_div,t2_v_div);
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_v_div=t2_v2_0; // TLM =
            v_div=v2_0; // SAC =
            t2_v2_0=t2_top_arg; // TLM scalar memory reference
            v2_0=top_arg; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t2_v2_0=t2_bottom_arg; // TLM scalar memory reference
            v2_0=bottom_arg; // SAC scalar memory reference
            t2_v1_1=t2_v2_0; // TLM =
            v1_1=v2_0; // SAC =
            t1_acs_div(v1_0,t2_v1_0,v1_1,t2_v1_1,v1_2,t2_v1_2);
            t2_v2_0=t2_a1_v_div; // TLM scalar memory reference
            v2_0=a1_v_div; // SAC scalar memory reference
            t2_a1_v1_2=t2_v2_0; // TLM =
            a1_v1_2=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_v_div=t2_v2_0; // TLM =
            a1_v_div=v2_0; // SAC =
            t1_acs_div(one,t2_one,v1_1,t2_v1_1,div_ret1,t2_div_ret1);
            t2_v2_0=t2_div_ret1; // TLM scalar memory reference
            v2_0=div_ret1; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_2; // TLM scalar memory reference
            v2_1=a1_v1_2; // SAC scalar memory reference
            t2_v2_2=v2_1*t2_v2_0+v2_0*t2_v2_1; // TLM *
            v2_2=v2_0*v2_1; // SAC *
            t2_a1_v1_0=t2_v2_2; // TLM =
            a1_v1_0=v2_2; // SAC =
            t2_v2_0=t2_v1_1; // TLM scalar memory reference
            v2_0=v1_1; // SAC scalar memory reference
            t2_v2_1=t2_v1_1; // TLM scalar memory reference
            v2_1=v1_1; // SAC scalar memory reference
            t2_v2_2=v2_1*t2_v2_0+v2_0*t2_v2_1; // TLM *
            v2_2=v2_0*v2_1; // SAC *
            t2_int_ret1=t2_v2_2; // TLM =
            int_ret1=v2_2; // SAC =
            t1_acs_div(v1_0,t2_v1_0,int_ret1,t2_int_ret1,div_ret2,t2_div_ret2);
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_v2_1=t2_div_ret2; // TLM scalar memory reference
            v2_1=div_ret2; // SAC scalar memory reference
            t2_v2_2=t2_a1_v1_2; // TLM scalar memory reference
            v2_2=a1_v1_2; // SAC scalar memory reference
            t2_v2_3=v2_2*t2_v2_1+v2_1*t2_v2_2; // TLM *
            v2_3=v2_1*v2_2; // SAC *
            t2_v2_4=t2_v2_0-t2_v2_3; // TLM -
            v2_4=v2_0-v2_3; // SAC -
            t2_a1_v1_1=t2_v2_4; // TLM =
            a1_v1_1=v2_4; // SAC =
            t2_v2_0=t2_a1_bottom_arg; // TLM scalar memory reference
            v2_0=a1_bottom_arg; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_1; // TLM scalar memory reference
            v2_1=a1_v1_1; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_bottom_arg=t2_v2_2; // TLM =
            a1_bottom_arg=v2_2; // SAC =
            t2_v2_0=t2_a1_top_arg; // TLM scalar memory reference
            v2_0=a1_top_arg; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_top_arg=t2_v2_2; // TLM =
            a1_top_arg=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_div(top_arg,t2_top_arg,bottom_arg,t2_bottom_arg,v_div,t2_v_div);
   }
}
void t2_a1_acs_sqrt(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_sqrt, double& t2_v_sqrt, double& a1_v_sqrt, double& t2_a1_v_sqrt)
#pragma ad indep arg t2_arg a1_arg t2_a1_arg a1_v_sqrt t2_a1_v_sqrt
#pragma ad dep v_sqrt t2_v_sqrt a1_arg t2_a1_arg
{
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   double div_ret1=0; 
   double t2_div_ret1=0; 
   double sqrt_ret1=0; 
   double t2_sqrt_ret1=0; 
   double int_ret1=0; 
   double t2_int_ret1=0; 
   double one=0; 
   double t2_one=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   t2_v2_0=0;  // TLM constant reference
   v2_0=1.0;  // SAC constant reference
   t2_one=t2_v2_0; // TLM =
   one=v2_0; // SAC =
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      // nonfloat assignment
      cs[cs_c]=0;
      // nonfloat assignment
      cs_c=cs_c+1;
      t2_v2_0=t2_v_sqrt; // TLM scalar memory reference
      v2_0=v_sqrt; // SAC scalar memory reference
      t2_fds[fds_c]=t2_v2_0; // TLM =
      fds[fds_c]=v2_0; // SAC =
      // nonfloat assignment
      fds_c=fds_c+1;
      t1_acs_sqrt(arg,t2_arg,v_sqrt,t2_v_sqrt);
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_v_sqrt=t2_v2_0; // TLM =
            v_sqrt=v2_0; // SAC =
            t2_v2_0=t2_arg; // TLM scalar memory reference
            v2_0=arg; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t1_acs_sqrt(v1_0,t2_v1_0,v1_1,t2_v1_1);
            t2_v2_0=t2_a1_v_sqrt; // TLM scalar memory reference
            v2_0=a1_v_sqrt; // SAC scalar memory reference
            t2_a1_v1_1=t2_v2_0; // TLM =
            a1_v1_1=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_v_sqrt=t2_v2_0; // TLM =
            a1_v_sqrt=v2_0; // SAC =
            t1_acs_sqrt(v1_0,t2_v1_0,sqrt_ret1,t2_sqrt_ret1);
            t2_v2_0=0;  // TLM constant reference
            v2_0=2;  // SAC constant reference
            t2_v2_1=t2_sqrt_ret1; // TLM scalar memory reference
            v2_1=sqrt_ret1; // SAC scalar memory reference
            t2_v2_2=v2_1*t2_v2_0+v2_0*t2_v2_1; // TLM *
            v2_2=v2_0*v2_1; // SAC *
            t2_int_ret1=t2_v2_2; // TLM =
            int_ret1=v2_2; // SAC =
            t1_acs_div(one,t2_one,int_ret1,t2_int_ret1,div_ret1,t2_div_ret1);
            t2_v2_0=t2_div_ret1; // TLM scalar memory reference
            v2_0=div_ret1; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_1; // TLM scalar memory reference
            v2_1=a1_v1_1; // SAC scalar memory reference
            t2_v2_2=v2_1*t2_v2_0+v2_0*t2_v2_1; // TLM *
            v2_2=v2_0*v2_1; // SAC *
            t2_a1_v1_0=t2_v2_2; // TLM =
            a1_v1_0=v2_2; // SAC =
            t2_v2_0=t2_a1_arg; // TLM scalar memory reference
            v2_0=a1_arg; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_arg=t2_v2_2; // TLM =
            a1_arg=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_sqrt(arg,t2_arg,v_sqrt,t2_v_sqrt);
   }
}
void t2_a1_acs_ln(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_ln, double& t2_v_ln, double& a1_v_ln, double& t2_a1_v_ln)
#pragma ad indep arg t2_arg a1_arg t2_a1_arg a1_v_ln t2_a1_v_ln
#pragma ad dep v_ln t2_v_ln a1_arg t2_a1_arg
{
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      // nonfloat assignment
      cs[cs_c]=0;
      // nonfloat assignment
      cs_c=cs_c+1;
      t2_v2_0=t2_v_ln; // TLM scalar memory reference
      v2_0=v_ln; // SAC scalar memory reference
      t2_fds[fds_c]=t2_v2_0; // TLM =
      fds[fds_c]=v2_0; // SAC =
      // nonfloat assignment
      fds_c=fds_c+1;
      t1_acs_ln(arg,t2_arg,v_ln,t2_v_ln);
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_v_ln=t2_v2_0; // TLM =
            v_ln=v2_0; // SAC =
            t2_v2_0=t2_arg; // TLM scalar memory reference
            v2_0=arg; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t1_acs_ln(v1_0,t2_v1_0,v1_1,t2_v1_1);
            t2_v2_0=t2_a1_v_ln; // TLM scalar memory reference
            v2_0=a1_v_ln; // SAC scalar memory reference
            t2_a1_v1_1=t2_v2_0; // TLM =
            a1_v1_1=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_v_ln=t2_v2_0; // TLM =
            a1_v_ln=v2_0; // SAC =
            t1_acs_div(a1_v1_1,t2_a1_v1_1,v1_0,t2_v1_0,a1_v1_0,t2_a1_v1_0);
            t2_v2_0=t2_a1_arg; // TLM scalar memory reference
            v2_0=a1_arg; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_arg=t2_v2_2; // TLM =
            a1_arg=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_ln(arg,t2_arg,v_ln,t2_v_ln);
   }
}
void t2_a1_acs_exp(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_exp, double& t2_v_exp, double& a1_v_exp, double& t2_a1_v_exp)
#pragma ad indep arg t2_arg a1_arg t2_a1_arg a1_v_exp t2_a1_v_exp
#pragma ad dep v_exp t2_v_exp a1_arg t2_a1_arg
{
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      // nonfloat assignment
      cs[cs_c]=0;
      // nonfloat assignment
      cs_c=cs_c+1;
      t2_v2_0=t2_v_exp; // TLM scalar memory reference
      v2_0=v_exp; // SAC scalar memory reference
      t2_fds[fds_c]=t2_v2_0; // TLM =
      fds[fds_c]=v2_0; // SAC =
      // nonfloat assignment
      fds_c=fds_c+1;
      t1_acs_exp(arg,t2_arg,v_exp,t2_v_exp);
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_v_exp=t2_v2_0; // TLM =
            v_exp=v2_0; // SAC =
            t2_v2_0=t2_arg; // TLM scalar memory reference
            v2_0=arg; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t1_acs_exp(v1_0,t2_v1_0,v1_1,t2_v1_1);
            t2_v2_0=t2_a1_v_exp; // TLM scalar memory reference
            v2_0=a1_v_exp; // SAC scalar memory reference
            t2_a1_v1_1=t2_v2_0; // TLM =
            a1_v1_1=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_v_exp=t2_v2_0; // TLM =
            a1_v_exp=v2_0; // SAC =
            t2_v2_0=t2_v1_1; // TLM scalar memory reference
            v2_0=v1_1; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_1; // TLM scalar memory reference
            v2_1=a1_v1_1; // SAC scalar memory reference
            t2_v2_2=v2_1*t2_v2_0+v2_0*t2_v2_1; // TLM *
            v2_2=v2_0*v2_1; // SAC *
            t2_a1_v1_0=t2_v2_2; // TLM =
            a1_v1_0=v2_2; // SAC =
            t2_v2_0=t2_a1_arg; // TLM scalar memory reference
            v2_0=a1_arg; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_arg=t2_v2_2; // TLM =
            a1_arg=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_exp(arg,t2_arg,v_exp,t2_v_exp);
   }
}
void t2_a1_acs_tanh(int bmode_1, double& arg, double& t2_arg, double& a1_arg, double& t2_a1_arg, double& v_tanh, double& t2_v_tanh, double& a1_v_tanh, double& t2_a1_v_tanh)
#pragma ad indep arg t2_arg a1_arg t2_a1_arg a1_v_tanh t2_a1_v_tanh
#pragma ad dep v_tanh t2_v_tanh a1_arg t2_a1_arg
{
   double v1_0=0; 
   double t2_v1_0=0; 
   double a1_v1_0=0; 
   double t2_a1_v1_0=0; 
   double v1_1=0; 
   double t2_v1_1=0; 
   double a1_v1_1=0; 
   double t2_a1_v1_1=0; 
   int save_cs_c=0; 
   double v2_0=0; 
   double t2_v2_0=0; 
   double v2_1=0; 
   double t2_v2_1=0; 
   double v2_2=0; 
   double t2_v2_2=0; 
   double v2_3=0; 
   double t2_v2_3=0; 
   double v2_4=0; 
   double t2_v2_4=0; 
   double v2_5=0; 
   double t2_v2_5=0; 
   double v2_6=0; 
   double t2_v2_6=0; 
   double v2_7=0; 
   double t2_v2_7=0; 
   // nonfloat assignment
   save_cs_c=cs_c;
   if (bmode_1==1) {
      // nonfloat assignment
      cs[cs_c]=0;
      // nonfloat assignment
      cs_c=cs_c+1;
      t2_v2_0=t2_v_tanh; // TLM scalar memory reference
      v2_0=v_tanh; // SAC scalar memory reference
      t2_fds[fds_c]=t2_v2_0; // TLM =
      fds[fds_c]=v2_0; // SAC =
      // nonfloat assignment
      fds_c=fds_c+1;
      t1_acs_tanh(arg,t2_arg,v_tanh,t2_v_tanh);
      while (cs_c>save_cs_c) {
         // nonfloat assignment
         cs_c=cs_c-1;
         if (cs[cs_c]==0) {
            // nonfloat assignment
            fds_c=fds_c-1;
            t2_v2_0=t2_fds[fds_c]; // TLM array memory reference
            v2_0=fds[fds_c]; // SAC array memory reference
            t2_v_tanh=t2_v2_0; // TLM =
            v_tanh=v2_0; // SAC =
            t2_v2_0=t2_arg; // TLM scalar memory reference
            v2_0=arg; // SAC scalar memory reference
            t2_v1_0=t2_v2_0; // TLM =
            v1_0=v2_0; // SAC =
            t1_acs_tanh(v1_0,t2_v1_0,v1_1,t2_v1_1);
            t2_v2_0=t2_a1_v_tanh; // TLM scalar memory reference
            v2_0=a1_v_tanh; // SAC scalar memory reference
            t2_a1_v1_1=t2_v2_0; // TLM =
            a1_v1_1=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=0;  // SAC constant reference
            t2_a1_v_tanh=t2_v2_0; // TLM =
            a1_v_tanh=v2_0; // SAC =
            t2_v2_0=0;  // TLM constant reference
            v2_0=1;  // SAC constant reference
            t2_v2_1=t2_v1_1; // TLM scalar memory reference
            v2_1=v1_1; // SAC scalar memory reference
            t2_v2_2=t2_v1_1; // TLM scalar memory reference
            v2_2=v1_1; // SAC scalar memory reference
            t2_v2_3=v2_2*t2_v2_1+v2_1*t2_v2_2; // TLM *
            v2_3=v2_1*v2_2; // SAC *
            t2_v2_4=t2_v2_0-t2_v2_3; // TLM -
            v2_4=v2_0-v2_3; // SAC -
            t2_v2_5=t2_v2_4; // TLM ()
            v2_5=v2_4; // SAC ()
            t2_v2_6=t2_a1_v1_1; // TLM scalar memory reference
            v2_6=a1_v1_1; // SAC scalar memory reference
            t2_v2_7=v2_6*t2_v2_5+v2_5*t2_v2_6; // TLM *
            v2_7=v2_5*v2_6; // SAC *
            t2_a1_v1_0=t2_v2_7; // TLM =
            a1_v1_0=v2_7; // SAC =
            t2_v2_0=t2_a1_arg; // TLM scalar memory reference
            v2_0=a1_arg; // SAC scalar memory reference
            t2_v2_1=t2_a1_v1_0; // TLM scalar memory reference
            v2_1=a1_v1_0; // SAC scalar memory reference
            t2_v2_2=t2_v2_0+t2_v2_1; // TLM +
            v2_2=v2_0+v2_1; // SAC +
            t2_a1_arg=t2_v2_2; // TLM =
            a1_arg=v2_2; // SAC =
         }
      }
   }
   if (bmode_1==2) {
      t1_acs_tanh(arg,t2_arg,v_tanh,t2_v_tanh);
   }
}
