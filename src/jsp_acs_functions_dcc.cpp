#include "EsoCommon_Header.hpp"

void jsp_acs_sum(jsp* x, int& len, jsp& sum_x)
#pragma ad indep x
#pragma ad dep sum_x
{
   int i=0; 
   for ( i=0; i<len; i=i+1) {
      sum_x=sum_x+x[i];
   }
}
void jsp_acs_pow(jsp& a, jsp& x, jsp& v_pow)
#pragma ad indep a x
#pragma ad dep v_pow
{
   int not_done=1; 
   jsp G=0; 
   jsp H=0; 
   jsp G_mal_2=0; 
   jsp G_mal_2_plus_1=0; 
   jsp abs_wert=0; 
   jsp v_pow_abs_exp=0; 
   jsp v_one=1.0; 
   if (x<0) {
      H=0-x;
      abs_wert=0-x;
   }
   else {
      H=x;
      abs_wert=x;
   }
   G=H/2;
   G_mal_2=G*2;
   G_mal_2_plus_1=G*2+1;
   if ((a==0) && (x==0) && (not_done)) {
      v_pow=assign(a,x,1);
      not_done=0;
   }
   if ((x<0) && (not_done)) {
      if ((a>=0) && (not_done)) {
         v_pow_abs_exp=pow(a,abs_wert);
         jsp_acs_div(v_one,v_pow_abs_exp,v_pow);
         not_done=0;
      }
      if ((a<0) && (not_done)) {
         if ((G_mal_2==H) && (not_done)) {
            v_pow_abs_exp=pow(a,abs_wert);
            jsp_acs_div(v_one,v_pow_abs_exp,v_pow);
            not_done=0;
         }
         if ((G_mal_2_plus_1==H) && (not_done)) {
            v_pow_abs_exp=pow(a,abs_wert);
            jsp_acs_div(v_one,v_pow_abs_exp,v_pow);
            not_done=0;
         }
         if (not_done==1) {
            cout << "POW NOT DEFINED for negativ argument and rational exponent" << endl;
         }
      }
   }
   if ((a==0) && (not_done)) {
      v_pow=assign(a,x,0);
      not_done=0;
   }
   if ((a<0) && (not_done)) {
      if ((G_mal_2==H) && (not_done)) {
         v_pow=pow(a,x);
         not_done=0;
      }
      if ((G_mal_2_plus_1==H) && (not_done)) {
         v_pow=pow(a,x);
         not_done=0;
      }
      if (not_done==1) {
         cout << "POW NOT DEFINED for negativ argument and rational exponent" << endl;
      }
   }
   else {
      if (not_done==1) {
         v_pow=pow(a,x);
      }
   }
}
void jsp_acs_div(jsp& top_arg, jsp& bottom_arg, jsp& v_div)
#pragma ad indep top_arg bottom_arg
#pragma ad dep v_div
{
   if (bottom_arg==0) {
      jsp constVal(0);
	  constVal = assign(bottom_arg,1e-020);
      v_div = top_arg/constVal;
   }
   else {
      v_div=top_arg/bottom_arg;
   }
}
void jsp_acs_sqrt(jsp& arg, jsp& v_sqrt)
#pragma ad indep arg
#pragma ad dep v_sqrt
{
   if (arg>=0) {
      v_sqrt=sqrt(arg);
   }
   else {
      v_sqrt=0-sqrt(0-arg);
   }
}
void jsp_acs_ln(jsp& arg, jsp& v_ln)
#pragma ad indep arg
#pragma ad dep v_ln
{
   if (arg>0) {
      v_ln=log(arg);
   }
   else {
	  jsp constVal(0);
	  constVal = assign(arg,1e-020);
      v_ln = constVal;
   }
}
void jsp_acs_exp(jsp& arg, jsp& v_exp)
#pragma ad indep arg
#pragma ad dep v_exp
{
	if(arg < 0-708)
	{
		jsp constVal(0);
	    constVal = assign(arg,0);
		v_exp = constVal;
	}
	else {
		if(arg > 709)
		{
			jsp constVal(0);
			constVal = assign(arg,1e308);
			v_exp = constVal;
		}
		else
		{
			v_exp = exp(arg);
		}
	}
}
void jsp_acs_tanh(jsp& x, jsp& v_tanh)
#pragma ad indep x
#pragma ad dep v_tanh
{
   v_tanh=tanh(x);
}
