/*! \file eso.cpp
 * \brief A ascii output of the values and names of an eso
 *
 *( C ) 2003, All rights reserved, Jutta Wyes
 * Lehrstuhl fuer Prozesstechnik, RWTH Aachen
 *
 * $Id: eso.cpp,v 1.2 2005/04/28 07:33:03 jwy Exp $
 */


#define MAKE_A_LptNumerics_DLL

#define MAX1000 90000

#include <cstdlib>
#include <iostream>
#include <string>
#include <vector>

#include "eso.hpp"
//#include "debug_message.hpp"



//using namespace LptNumerics::Utility;
using namespace LptNumerics::Eso;

using namespace std;

ostream& LptNumerics::Eso::Eso::output_to_stream( ostream& s, const char* name,
    short format )
{
 	s.setf(ios_base::scientific, ios_base::floatfield);
	s.precision(16);
   //DebugMessage deb( "LptNumerics::Eso::Eso::output_to_stream" );
    //	deb.set_global_level(DM_SILENT);//DM_METHOD_CALL);
    // declarations
    long nv;
		unsigned k;

    nv =  get_num_vars();
    double* val = new double[nv];
    double* der = new double[nv];

    long ne =  get_num_eqns();
    double* res = new double[ne];

    vector < string > names;


    s << name << " -------------- Jacobian structure ------------------"
        << endl;

    // get jacobian entries for output
    long nz=0;
		short has_info=has_jacobian_info();
    if (has_info>0)
			nz =  get_num_jacobian_entries();
    s << "0-no-info, 1-not-all-values, 2-all-values info:";
		s	<<  has_info << endl;
     get_variables( nv, NULL, val );
     get_derivatives( nv, NULL, der );
     get_variable_names( names );

    s << "Number of variable names: " << names.size() << endl;

    // get the structure
    long* rows = new long[nz];
    long* cols = new long[nz];
    short* computed = new short[nz];
    if (has_info>0) 
			get_jacobian_struct( nz, rows, cols, computed );
    // get all jacobian values
    double* jacs = new double[nz];
		long num_comp=nz; 
		if (has_info==2)
      get_jacobian_values( nz, NULL, jacs );
		else if (has_info==1)
		{
			long j=0;
  		long* comp_indices=new long[nz];
			for (long i=0; i<nz; i++)
				if (computed[i]==1)
					comp_indices[j++]=i;
			num_comp=j; // number of computable non zeroes
      //get_jacobian_values(num_comp, comp_indices, jacs );
			delete [] comp_indices;
		}

    // cut the output to maximal MAX1000
    s << "number of jacobian entries: " << nz << endl;
    s << "number of computable jacobian entries: " << num_comp << endl;

    if( nz < MAX1000 )
    {
        k = nz;
    }
    else
    {
        k = MAX1000;
        s << "output of the first MAX1000 jacobian values" << endl;
    }
		
		long nc=0;
		if (has_info>0)
    for( unsigned j = 0; j < k; j++ )
    {
        s << j << ": (" << rows[j] << ",";
        s << cols[j] << " ";
        if( cols[j] < (long) names.size())
        {
            s << names[cols[j]];
        }
        else
        {
            s << "<no varname>";
        }
        s << ")";
				if (computed[j]==1)
					s << " = " << jacs[nc++] << endl;
				else 
					s << " not computable" << endl;
    }

    s << endl << endl;

    delete [] jacs;
    delete [] rows;
    delete [] cols;
    delete [] computed;

    // output

    s << name << " -------- Differential"" Jacobian structure -----------"
        << endl;

    // get jacobian entries for output
    long mz;
		long has_diff_info = has_diff_jacobian_info();
    if (has_diff_info >0)
      mz =  get_num_diff_jacobian_entries();
		else 
			mz = 0;

    // define parameters
    long* d_rows = new long[mz];
    long* d_cols = new long[mz];
    short* d_computed = new short[mz];
    // get the structure

    if ( has_diff_info>0)
     get_diff_jacobian_struct( mz, d_rows, d_cols, d_computed );

    // define parameter for jacobian values
    double* diff_jacs = new double[mz];
    // get all jacobian values

    if ( has_diff_info==2)
			get_diff_jacobian_values( mz, NULL, diff_jacs );
    s << "0-no-info, 1-not-all-values, 2-all-values info:" <<
        has_diff_info << endl;
		s << "(0) not computable, (1) computable" << endl;

    // cut the output to maximal MAX1000
    s << "number of differential jacobian entries: " << mz << endl;
    if( mz < MAX1000 )
    {
        k = mz;
    }
    else
    {
        k = MAX1000;
        s << "output of the first MAX1000"" differential jacobian values" <<
            endl;
    }

    if ( has_diff_info>0)
    for( unsigned j = 0; j < k; j++ )
    {
        s << j << ": (" << d_rows[j] << "," << d_cols[j] << " ";
        if( d_cols[j] < (long) names.size())
        {
            s << names[d_cols[j]];
        }
        else
        {
            s << "<no varname>";
        }
        s << ")" ;
				if (d_computed[j]==1)
					s << " = " << diff_jacs[j] << endl;
				else 
					s << " not computable" << endl;
    }

    s << endl << endl;

    delete [] d_rows;
    delete [] d_cols;
    delete [] d_computed;
    delete [] diff_jacs;
 /*   // output
    s << name << " ----------------- Bounds"
        " ------------------------------------------" << endl;
    double* upp = new double[nv];
    double* low = new double[nv];
    get_bounds( nv, NULL, low, upp );
    if( nv < MAX1000 )
    {
        k = nv;
    }
    else
    {
        k = MAX1000;
    }
    for( j = 0; j < k; j++ )
    {
        s << j << ": " << names[j] << " upp: " << upp[j] << "  low:" <<
            low[j] << endl;
    }
    s << endl << flush;

    delete [] upp;
    delete [] low;
*/
    s << name << " ----------------- Variable and"
        " Derivative Values ------------------" << endl;

    s << "Number of variables: " << nv << " (+ independent_var)" << endl;
    if( nv < MAX1000 )
    {
        k = nv;
    }
    else
    {
        k = MAX1000;
    }
    s << "independent_var : " <<  get_independent_var() << endl;
    for( unsigned j = 0; j < k; j++ )
    {
        s << j << ": ";
        if( j < names.size())
        {
            s << names[j];
        }
        else
        {
            s << "<no varname>";
        };
				s << " : " << val[j] << "  der(";
        if( j < names.size())
        {
            s << names[j];
        }
        else
        {
            s << "<no varname>";
        };
				s << ") : " << der[j] << endl;
    }
    s << endl << flush;

    ne =  get_num_eqns();

     get_residuals( ne, NULL, res );

    if( ne < MAX1000 )
    {
        k = ne;
    }
    else
    {
        k = MAX1000;
    }
    s << name << " -------------- Residuals ""of Equations --------------"
        << endl;
    s << "Number of equations: " << ne << endl;
    for( unsigned j = 0; j < k; j++ )
    {
        s << "Equation " << j << " : " << res[j] << endl;
    }
    s << endl << endl << endl << flush;

    delete [] val;
    delete [] res;
    delete [] der;
    names.clear();

    return s;
}

//-------------------------------------------------------------------------
//
//-------------------------------------------------------------------------

ostream& operator << ( ostream& out, LptNumerics::Eso::Eso* eso )
{
    //DebugMessage deb( "LptNumerics::Eso::Eso::<<" );

    
    eso->output_to_stream( out,  /*eso,*/"",  /*format text*/1 );

    return out;

}

//-------------------------------------------------------------------------
//
//-------------------------------------------------------------------------

void LptNumerics::Eso::get_indices_of_names_in_eso( const vector < string >
    * gen_el, LptNumerics::Eso::Eso* eso, long* indices, long& len )
{
    //DebugMessage deb( "LptNumericsEso::get_indices_of_names_in_eso" );
    // get names of eso
    long nv = eso->get_num_vars();
    vector < string > eso_names;
    eso_names.reserve( nv );
    eso->get_variable_names( eso_names );

    unsigned num_names = gen_el->size();

    long i;
    len = 0;
    for( unsigned z = 0; z < num_names; z++ )
    {
        for( i = 0; i < nv; i++ )
        {
            if( eso_names[i] == ( *gen_el )[z] )
            {
                indices[len] = i;
                len++;
                i = nv * 2; // Abbruch, Name gefunden
            }
        }
        if( i == nv )
        {
            //cout << "LptNumericsEso::get_indices_of_names_in_eso " << 
            //    " not found " << ( *gen_el )[z] << " in ESO" << endl;
            indices[len] =  - 1;
            len++;
        }
    }
}

void Eso::get_var_nums(vector<string> names, long * indices)
{
	//DebugMessage deb( "Eso::get_var_nums");
	cout << "Eso::get_var_nums: not implemented" << endl;
	exit(1);
};

long Eso::get_num_assigned_vars() const
{
	//DebugMessage deb( "Eso::get_num_assigned_vars");
	cout << "Eso::get_num_assigned_vars: not implemented" << endl;
	exit(1);
};
 
long Eso::get_num_params() const
{
	cout << "Eso::get_num_assigned_vars: not implemented" << endl;
	exit(1);
};

void Eso::get_param_indices(long * indices) const
{
	cout << "Eso::get_param_indices not implemented" << endl;
	exit(1);
};

void Eso::get_var_sensitivities(double* sens)
{
	//DebugMessage deb( "Eso::get_var_sensitivities");
	cout << "Eso::get_var_sensitivities not implemented" << endl;
	exit(1);
};

void Eso::set_var_sensitivities(double* sens)
{
	//DebugMessage deb( "Eso::set_var_sensitivities");
	cout << "Eso::set_var_sensitivities not implemented" << endl;
	exit(1);
};

long Eso::get_num_diff_vars()
{
//got from get_diff_jacobian_struct 
	//DebugMessage deb( "Eso::get_num_diff_vars");
	cout << "Eso::get_num_diff_vars not implemented" << endl;
	exit(1);
};

void Eso::get_diff_var_indices(long * indices)
{
	//DebugMessage deb( "Eso::get_diff_var_indices");
	cout << "Eso::get_diff_var_indices not implemented" << endl;
	exit(1);
};

void Eso::get_assigned_variables(long * indices)
{
// aus car.value.xml destination="eso" view="parameter" holen
	//DebugMessage deb( "Eso::get_assigned_variables");
	cout << "Eso::get_assigned_variables not implemented" << endl;
	exit(1);
};

void Eso::get_eqn_names(long len, long* indices, vector<string>& names)
{
	//DebugMessage deb( "Eso::get_eqn_names");
	cout << "Eso::get_eqn_names not implemented" << endl;
	exit(1);
};

long Eso::get_num_stns()
{
	//DebugMessage deb( "Eso::get_num_stns");
	return 0;
};

		 // input: multivector_gradient is given from integrator 
		 //				 with size = nv x np, nv is number of variables
		 //				 np: number of parameters
		 // output: directional_deriv directional derivative with size = ne x np,
		 //        ne is number of equations
		 // directional_deriv := jacobian values x multivector
void Eso::get_jac_mult_multivector(
			 double* multivector_gradient,
			 long np,
			 double* directional_deriv)
{
	//DebugMessage deb("Eso::get_jac_mult_multivector");
	cout << "Eso::get_jac_mult_multivector not implemented" << endl;
	exit(1);
};

// multivector * Hesse * multivector + Jac * multimatrix
void Eso::get_hesse_mult_multivector(
	double* first_order_sensitivities, // input dx/dp
	long np, // input number of parameters
	double* second_order_sensitivities, // input d2x/dpdq
	double* two_directional_derivs //output d2F/dpdq
){
	cout << "Eso::get_hesse_mult_multivector not implemented" << endl;
	exit(1);
};

//-------------------------------------------------------------------------
//
//-------------------------------------------------------------------------

#undef MAX1000
