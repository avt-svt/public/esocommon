#include "EsoCommon_Header.hpp"

bool cond_lock(int index, int* condit, int* lock, int* prev) {
  bool retval;
  if (lock[index])
    retval = (prev[index] != 0);
  else {
    retval = (condit[index]!=0);
    prev[index] = static_cast<int>(retval);
  }
  return retval;
}
