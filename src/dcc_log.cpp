#ifndef _dcc_log_
#define _dcc_log_

#include "EsoCommon_Header.hpp"

double dcc_log(double& x)
{
	if (x<=0){
#ifndef JADE_SILENT
		cout << "LOG NOT DEFINED" << endl;
#endif
		return 1e-020;
	}
	else{
		return log(x);
	}
}

#endif
