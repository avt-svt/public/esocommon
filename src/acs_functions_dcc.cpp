#include "EsoCommon_Header.hpp"

void acs_sum(double* x, int& len, double & sum_x) 
  #pragma ad indep x
  #pragma ad dep sum_x
{
	int i=0;
	for (i=0; i<len; i=i+1) { 
		sum_x  = sum_x + x[i];
	}
}
void acs_pow(double & a, double & x, double & v_pow)
  #pragma ad indep a x
  #pragma ad dep v_pow
{
	int not_done=1;
	int G=0;
 	double H=0;
 	double G_mal_2=0;
 	double G_mal_2_plus_1=0;
 	double abs_wert=0;
    double v_pow_abs_exp=0;
    double v_one = 1.0;
 
	if (x<0){H = 0-x; abs_wert = 0-x;}
	else{H = x; abs_wert = x;} 

 	G = (int)H/2; // to test later if H is rational or integer 
 	G_mal_2 = G*2;  // double (G*2)
	G_mal_2_plus_1 = G*2+1; // double G*2+1

	if((a==0) && (x==0) && (not_done))
	{
		v_pow = 1;
		not_done = 0;	
	}
	if ((x<0) && (not_done)) {
		if ((a>=0) && (not_done))
		{
			v_pow_abs_exp = pow(a, abs_wert);
			acs_div(v_one, v_pow_abs_exp, v_pow);
			not_done=0;
		}
		if ((a<0) && (not_done))// for a<0 pow not defined for rational exponent
		{
			if ((G_mal_2==H)  && (not_done)){// x gerade ganze Zahl
				v_pow_abs_exp = pow(a, abs_wert);
				acs_div(v_one, v_pow_abs_exp, v_pow);
				not_done=0;
			}
			if ((G_mal_2_plus_1==H) && (not_done)){// x ungerade ganze Zahl
				v_pow_abs_exp = pow(a, abs_wert);
				acs_div(v_one, v_pow_abs_exp, v_pow);
				not_done=0;
 			}
			if(not_done==1)
			{
#ifndef JADE_SILENT
				cout << "POW NOT DEFINED for negativ argument and rational exponent" << endl;
#endif
			}
		}
	}
	if ((a==0) && (not_done)) {v_pow = 0; not_done=0;}
	if ((a<0) && (not_done))// for a<0 pow not defined for rational exponent
	{
		if ((G_mal_2==H)  && (not_done)){// x gerade ganze Zahl
			v_pow = pow(a, x);
 			not_done=0;
		}
		if ((G_mal_2_plus_1==H) && (not_done)){// x ungerade ganze Zahl
			v_pow = pow(a, x);
 			not_done=0;
 		}
		if(not_done==1)
		{
#ifndef JADE_SILENT
			cout << "POW NOT DEFINED for negativ argument and rational exponent" << endl;
#endif
		}
	}
	else{
		if(not_done==1){v_pow = pow(a, x);}
	}
}

void acs_div(double & top_arg, double & bottom_arg, double & v_div)
  #pragma ad indep top_arg bottom_arg
  #pragma ad dep v_div
{
	if(bottom_arg == 0)
	{
		v_div = top_arg / 1e-020; // CapeMLEso::min_number;
	}
	else
	{
		v_div = top_arg / bottom_arg;
	}
}
void acs_sqrt(double & arg, double & v_sqrt)
  #pragma ad indep arg 
  #pragma ad dep v_sqrt
{
	//double a1 = 1.523437500000002e-7;
	//double a2 = -9.140625000000004e-8;
	//double a3 = 5.078124999999997e-8;
	//double a4 = -1.171874999999999e-8;
	//double xref = 1e-14;
	//double y = 0;
	//double y2 = 0;

	//if(arg >= 0)
	//{
	//	if(arg >= 1e-14)
	//		v_sqrt = sqrt(arg);
	//	else{ // interpolation of sqrt(x) between -1e-14 and 1e14 with 
	//		  // p(x)=a1*(x/xref) + a2*(x/xref)^3 + a3*(x/xref)^5 + a4*(x/xref)^7
	//		  // (found by Hermitian interpolation) 
	//		y = arg/xref;
	//		y2 = y*y;
	//		v_sqrt = a4*y2;
	//		v_sqrt = y2*(a3+v_sqrt);
	//		v_sqrt=y2*(a2+v_sqrt);
	//		v_sqrt=y*(a1+v_sqrt);
	//	}
	//}
	//else
	//{
	//	if(arg <= -1e-14) // mirroring of sqrt(x) in 0
	//		v_sqrt = -sqrt(-arg);
	//	else{
	//		y = arg/xref;
	//		y2 = y*y;
	//		v_sqrt = a4*y2;
	//		v_sqrt = y2*(a3+v_sqrt);
	//		v_sqrt=y2*(a2+v_sqrt);
	//		v_sqrt=y*(a1+v_sqrt);
	//	}
	//}
	if(arg >= 0){
		v_sqrt = sqrt(arg);
	}
	else{
		v_sqrt=-sqrt(-arg);
	}
}
void acs_ln(double & arg, double & v_ln)
  #pragma ad indep arg
  #pragma ad dep v_ln
{
	if(arg > 0)
	{
		v_ln = log(arg); 
	}
	else
	{
		v_ln = 1e-020; // CapeMLEso::min_number;
	}
}
void acs_exp(double & arg, double & v_exp)
  #pragma ad indep arg
  #pragma ad dep v_exp
{
	if(arg < 0-708)
	{
		v_exp = 0;
	}
	else {
		if(arg > 709)
		{
			v_exp = 1e308;
		}
		else
		{
			v_exp = exp(arg);
		}
	}
}
void acs_tanh(double & x, double & v_tanh)
  #pragma ad indep x
  #pragma ad dep v_tanh
{
	v_tanh = tanh(x);
}
